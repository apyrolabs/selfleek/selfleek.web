import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'demo/src/environments/environment';
import { ISelfiePreview } from './fleek-feed.interface';

@Injectable({
  providedIn: 'root'
})
export class FleekFeedService {

  constructor(private httpClient: HttpClient) { }

  getLatest(count:number) {
      const url = `${environment.apiUrl}/selfies/latest?count=${count}`
      return this.httpClient.get<ISelfiePreview[]>(url)
  }
}