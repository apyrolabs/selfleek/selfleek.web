import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FleekFeedComponent } from './fleek-feed.component';

describe('FleekFeedComponent', () => {
  let component: FleekFeedComponent;
  let fixture: ComponentFixture<FleekFeedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FleekFeedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FleekFeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
