import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FleekFeedComponent } from './fleek-feed/fleek-feed.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { FooterComponent } from './footer/footer.component';
import { SelfiePreviewComponent } from './fleek-feed/selfie-preview/selfie-preview.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import {MatBadgeModule} from '@angular/material/badge'; 
import { FleekFeedService } from './fleek-feed/fleek-feed.service';
import { HttpClientModule } from '@angular/common/http';
import {MatButtonModule} from '@angular/material/button';
@NgModule({
  declarations: [
    AppComponent,
    FleekFeedComponent,
    ToolbarComponent,
    FooterComponent,
    SelfiePreviewComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatBadgeModule,
    HttpClientModule,
    MatButtonModule
  ],
  providers: [
    FleekFeedService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
