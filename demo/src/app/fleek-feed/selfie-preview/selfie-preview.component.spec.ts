import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelfiePreviewComponent } from './selfie-preview.component';

describe('SelfiePreviewComponent', () => {
  let component: SelfiePreviewComponent;
  let fixture: ComponentFixture<SelfiePreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelfiePreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelfiePreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
