export interface ISelfiePreview {
    ID:string;
    ImageURL:string;
    Caption:string;
    CreatedAt:string;
}