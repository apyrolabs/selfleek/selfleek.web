import { Component, OnInit, Input } from '@angular/core';
import { ISelfiePreview } from '../fleek-feed.interface';

@Component({
  selector: 'app-selfie-preview',
  templateUrl: './selfie-preview.component.html',
  styleUrls: ['./selfie-preview.component.css']
})
export class SelfiePreviewComponent implements OnInit {

  @Input() selfie:ISelfiePreview
  
  constructor() { }

  ngOnInit() {
  }

}
