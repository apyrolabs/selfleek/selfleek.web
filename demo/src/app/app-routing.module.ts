import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FleekFeedComponent } from './fleek-feed/fleek-feed.component';


const routes: Routes = [
  {path:"", component:FleekFeedComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
