import { Component, OnInit } from '@angular/core';
import { FleekFeedService } from './fleek-feed.service';
import { ISelfiePreview } from './fleek-feed.interface';

@Component({
  selector: 'app-fleek-feed',
  templateUrl: './fleek-feed.component.html',
  styleUrls: ['./fleek-feed.component.css']
})
export class FleekFeedComponent implements OnInit {

  constructor(private feed: FleekFeedService) { }

  selfies: ISelfiePreview[] = [];

  ngOnInit() {
    this.feed.getLatest(10).subscribe(
      data => this.selfies=data
    )
  }

}
